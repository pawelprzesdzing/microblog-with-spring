package pl.sda.przesdzing.SpringApp.Config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import pl.sda.przesdzing.SpringApp.DAO.RoleRepository;
import pl.sda.przesdzing.SpringApp.DAO.UserRepository;
import pl.sda.przesdzing.SpringApp.Model.Role;
import pl.sda.przesdzing.SpringApp.Model.User;

import java.util.HashSet;

@Configuration
@EnableWebSecurity
@Slf4j
public class WebConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver =
                new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Qualifier("userDetailsServiceImpl")
    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    CommandLineRunner loadDataBase(UserRepository userRepository, RoleRepository roleRepository) {
        return args -> {
            Role admin = new Role();
            admin.setName("ADMIN");
            Role user = new Role();
            user.setName("USER");
            roleRepository.save(admin);
            roleRepository.save(user);

            User user1 = new User("Pawel", "Przesdzing", "admin", "pp@pp.pl", bCryptPasswordEncoder.encode("password"), new HashSet<>(roleRepository.findAll()));
            User user2 = new User("Dominika", "Przesdzing", "user1", "dp@dp.pl", bCryptPasswordEncoder.encode("password"), new HashSet<>(roleRepository.findByName("USER")));
            User user3 = new User("Jan", "Kowalski", "user2", "dp@dp.pl", bCryptPasswordEncoder.encode("password"), new HashSet<>(roleRepository.findByName("USER")));

            log.info(String.valueOf(userRepository.save(user1)));
            log.info(String.valueOf(userRepository.save(user2)));
            log.info(String.valueOf(userRepository.save(user3)));
        };
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        SimpleUrlAuthenticationSuccessHandler authSuccessHandler = new SimpleUrlAuthenticationSuccessHandler();
        authSuccessHandler.setUseReferer(true);
        http
                .authorizeRequests()
                .antMatchers("/resources/**", "/registration", "/home", "/login", "/", "/post/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/home", true)
                .permitAll()
                .and()
                .logout()
                .logoutSuccessUrl("/home")
                .permitAll();


    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/css/**");
    }

}

