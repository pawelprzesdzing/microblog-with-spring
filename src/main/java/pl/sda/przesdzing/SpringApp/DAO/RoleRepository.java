package pl.sda.przesdzing.SpringApp.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.przesdzing.SpringApp.Model.Role;

import java.util.HashSet;

public interface RoleRepository extends JpaRepository<Role, Long> {
    HashSet<Role> findByName(String name);
}
