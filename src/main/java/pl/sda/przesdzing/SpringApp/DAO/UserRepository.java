package pl.sda.przesdzing.SpringApp.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.przesdzing.SpringApp.Model.User;

public interface UserRepository extends JpaRepository<User, Long>{
    User findByUsername(String nickname);
}
