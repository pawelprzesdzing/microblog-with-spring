package pl.sda.przesdzing.SpringApp.Service;

import pl.sda.przesdzing.SpringApp.Model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
