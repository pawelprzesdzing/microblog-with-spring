package pl.sda.przesdzing.SpringApp.Service;

public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}
