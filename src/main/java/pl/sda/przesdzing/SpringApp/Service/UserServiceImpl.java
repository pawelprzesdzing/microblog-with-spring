package pl.sda.przesdzing.SpringApp.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.przesdzing.SpringApp.DAO.RoleRepository;
import pl.sda.przesdzing.SpringApp.DAO.UserRepository;
import pl.sda.przesdzing.SpringApp.Model.User;

import java.util.HashSet;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        if(user.getPassword().length() < 32) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        }
        if(user.getRoles() == null) {
            user.setRoles(new HashSet<>(roleRepository.findByName("USER")));
        }
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}
