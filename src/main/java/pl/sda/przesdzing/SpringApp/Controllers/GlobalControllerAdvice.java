package pl.sda.przesdzing.SpringApp.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import pl.sda.przesdzing.SpringApp.Model.User;
import pl.sda.przesdzing.SpringApp.Service.UserService;

import java.security.Principal;

@ControllerAdvice
public class GlobalControllerAdvice {

    @Autowired
    private UserService userService;

    @ModelAttribute()
    public void loggedInUser(Model model, Principal principal) {
        if(principal != null){
            User user = userService.findByUsername(principal.getName());
            model.addAttribute("user", user);
        }
    }
}
