<%@ page import="java.time.LocalDate" %><%--
  Created by IntelliJ IDEA.
  User: Paweł
  Date: 07.06.2019
  Time: 13:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/css.css">
<html>
<head>
    <title>Register</title>
    <jsp:include page="header.jsp"/>
</head>
<body>
<div id="container">
    <style>
        body {
            background-image: url("https://images.pexels.com/photos/1308624/pexels-photo-1308624.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500");
        }
    </style>

    <div id="logo">
        <h1>Rental Office</h1>
    </div>

    <div id="nav">
        <h1 class="display-3 text-own" align="center">Books</h1><br>
        <button class="button">Testament</button>
        <button class="button">Pan Tadeusz</button>
        <button class="button">Władca Pierścieni: Drużyna pierścienia</button>
        <button class="button">Kasacja</button>
        <button class="button">Harry Potter i Komnata Tajemnic</button>
    </div>

    <br id="content" style="background-color: white">
    <div align="center">
        <form action="${pageContext.request.contextPath}/register" method="post">
            <h1 class="display-3 text-own">Register</h1><br>
            <div class="form-group">
                <input class="form-control" style="width:30ex" placeholder="Enter nickname" type="text"
                       name="nickname"/>
            </div>
            <div class="form-group">
                <input class="form-control" style="width:30ex" placeholder="Enter name" type="text"
                       name="name"/>
            </div>
            <div class="form-group">
                <input class="form-control" style="width:30ex" placeholder="Enter surname" type="text" name="surname"/>
            </div>
            <div class="form-group">
                <input class="form-control" style="width:30ex" placeholder="Enter email" type="text" name="email"/>
            </div>
            <div class="form-group">
                <input class="form-control" style="width:30ex" placeholder="Enter password" type="password"
                       name="password"/>
            </div>
            <p>
                <input class="btn btn-primary" type="submit" name="submit" value="Register"/>
            </p>
        </form>
    </div>
    </br>
</div>
</body>
<jsp:include page="footer.jsp"/>
</html>
